<?php

class UserCourse extends Eloquent{
	protected $table = "users_courses";

	public function users()
	{
		return $this->belongsTo('User');
	}	

}
