<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent  implements UserInterface, RemindableInterface{
	
	protected $primaryKey = 'id';
	
	public function users_courses()
	{
		return $this->hasMany('UserCourse');
	}
	
	public function getFullName()
	{
		return $this->first_name.' '.$this->last_name;
	}
	
	public function isAdmin()
	{
		
		if ($this->permissions == 1){
			
			return true;
			
		}
		
		else 
			return false;
		
	}
	
	
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}
	
	public function getAuthPassword()
	{
		return $this->password;
	}
	
	public function getReminderEmail()
	{
		return $this->email; 
	}
	
	
	public function getRememberToken()
	{
		return $this->remember_token;
	}
	
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}
	
	public function getRememberTokenName()
	{
		return 'remember_token';
	}
	
}