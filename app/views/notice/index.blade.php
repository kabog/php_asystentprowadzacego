@extends('layouts.master')
 
@section('title') Ogłoszenia @stop
 
@section('content')


<div class='col-lg-4 col-lg-offset-4'>
 
    {{ Form::open(['role' => 'form', 'url' => '/notice']) }}
 
    <div class='form-group'>
        {{ Form::label('content', 'Treść ogłoszenia') }}
        {{ Form::text('content', null, ['placeholder' => 'Treść ogłoszenia', 'class' => 'form-control']) }}
    
    </div>

    <div class='form-group'>
        {{ Form::submit('Dodaj ogłoszenie', ['class' => 'btn btn-primary']) }}
        <a href="/" class="btn btn-info">Powrót na stronę główną</a>
    </div>
   
    {{ Form::close() }}
 
</div>
 
@stop