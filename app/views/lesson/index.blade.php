@extends('layouts.master')
 
@section('title') Lekcja @stop
 
@section('content')
 
<div class="col-lg-10 col-lg-offset-1">
 
    <h1><i class="fa fa-heart"></i> Lekcja: {{$lesson->name}} <a href="/" class="btn btn-info pull-right">Powrót na stronę główną</a></h1>
 	
{{$lesson->content}}

@if($attachmentExist)
	<a href="{{URL::asset($file)}}">Załącznik do lekcji.</a>
@endif
	
@stop