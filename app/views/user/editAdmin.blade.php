@extends('layouts.master')
 
@section('title') Edycja konta administratora @stop
 
@section('content')
 
<div class='col-lg-4 col-lg-offset-4'>
 
    @if ($errors->has())
        @foreach ($errors->all() as $error)
            <div class='bg-danger alert'>{{ $error }}</div>
        @endforeach
    @endif

 
    <h1><i class='fa fa-user'></i> Edycja Twojego konta </h1>
 
    <!--{{ Form::open(['role' => 'form', 'url' => '/user']) }}  -->
 	{{ Form::model($user, ['role' => 'form', 'url' => '/user/' . $user->id, 'method' => 'PUT']) }}
 
    <div class='form-group'>
        {{ Form::label('$user->first_name', 'Imię') }}
        {{ Form::text('first_name', $user->first_name ,['placeholder' => 'Nowe imię', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('last_name', 'Nazwisko') }}
        {{ Form::text('last_name', null , ['placeholder' => 'Nazwisko', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('login', 'Login') }}
        {{ Form::text('login', $user->login, ['placeholder' => 'Login', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', $user->email, ['placeholder' => 'Email', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('password', 'Hasło') }}
        {{ Form::password('password', ['placeholder' => 'Hasło', 'class' => 'form-control']) }}
    </div>
 
 
    <div class='form-group'>
        {{ Form::submit('Edytuj', ['class' => 'btn btn-primary']) }}
         <a href="/user" class="btn btn-info">Powrót</a>
    </div>
 
    {{ Form::close() }}
    
 
</div>
@stop