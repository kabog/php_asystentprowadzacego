@extends('layouts.master')
 
@section('title') Users @stop
 
@section('content')
 
<div class="col-lg-10 col-lg-offset-1">
 
    <h1><i class="fa fa-users"></i> Obsługa użytkowników<a href="/" class="btn btn-default pull-right">Powrót na stronę główną</a></h1>
 	
 	
 	Zalogowany jako {{ Auth::user()->getFullName(); }}
    
    
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
 
            <thead>
                <tr>
                    <th>Imnię i nazwisko</th>
                    <th>Email</th>
                    <th>Login</th>
                    <th>Kierunek studiów</th>
                    <th></th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{ $user->getFullName() }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->login }}</td>
                    <td>{{ $user->specialization }}</td>
                    <td>
                    @if ($user->permissions == 1)
                        <a href="/user/{{ $user->id }}/edit" class="btn btn-info pull-left" style="margin-right: 3px;">Edytuj</a>
                    @endif
                        @if( !$user->active)
                        	{{ Form::open(['url' => '/activate/'. $user->id, 'method' => 'GET']) }}
	                        {{ Form::submit('Aktywuj', ['class' => 'btn btn-primary pull-left', 'style'=> 'margin-right: 3px;' ])}}
	                        {{ Form::close() }}
                        @else 
                        	 <div class="btn btn-success pull-left" style="margin-right: 3px;">Aktywny</div>
                        @endif
                         
                         
                        @if( $user->permissions != 1)
                        {{ Form::open(['url' => '/user/' . $user->id, 'method' => 'DELETE']) }}
                        {{ Form::submit('Usuń', ['class' => 'btn btn-danger'])}}
                        {{ Form::close() }}
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
 
        </table>
    </div>
 
</div>
 
@stop