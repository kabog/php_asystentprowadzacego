@extends('layouts.master')
 
@section('title') Users @stop
 
@section('content')
 
<div class="col-lg-10 col-lg-offset-1">
 
    <h1><i class="fa fa-users"></i> Kursy <a href="/" class="btn btn-default pull-right">Powrót na stronę główną</a></h1>
 	
 	Zalogowany jako {{ Auth::user()->getFullName(); }}
    
    
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
 
            <thead>
                <tr>
                    <th>Nazwa kursu</th>
                    <th>Lekcje</th>
                    <th>Edycja</th>
                </tr>
            </thead>
 
            <tbody>
                @foreach ($courses as $course)
                <tr>
                    <td>{{ $course->name }}
                        {{ Form::open(['url' => '/course/' . $course->id, 'method' => 'DELETE']) }}
                        {{ Form::submit('Usuń', ['class' => 'btn btn-danger'])}}
                        {{ Form::close() }}
                    </td>
                    
                    <td>
                   @foreach ($lessons as $lesson)
                   @if ($lesson->course_id == $course->id)
                   <li>
                   {{ $lesson->name }}
                    {{ Form::open(['url' => '/lesson/' . $lesson->id, 'method' => 'DELETE']) }}
                    {{ Form::submit('Usuń', ['class' => 'btn btn-danger btn-xs'])}}
                    {{ Form::close() }}
                   </li>
                   @endif
                   @endforeach
                    </td>
                    
                    <td>
                        <a href="/course/{{ $course->id }}/edit" class="btn btn-info pull-left" style="margin-right: 3px;">Dodaj lekcję</a>
                        
                    </td>

                </tr>
                @endforeach
            </tbody>
 
        </table>
        
        <a href="/course/create" class="btn btn-info pull-right" style="margin-right: 3px;">Dodaj kurs</a>
                        
        
    </div>
 
</div>
 
@stop