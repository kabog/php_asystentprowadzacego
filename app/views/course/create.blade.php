
@extends('layouts.master')
 
@section('title') Dodaj kurs @stop
 
@section('content')


<div class='col-lg-4 col-lg-offset-4'>
 
    <h1><i class='fa fa-user'></i> Dodaj kurs</h1>

 
    {{ Form::open(['role' => 'form', 'url' => '/course']) }}
 
    <div class='form-group'>
        {{ Form::label('name', 'Nazwa') }}
        {{ Form::text('name', null, ['placeholder' => 'Nazwa', 'class' => 'form-control']) }}
    </div>

 
    <div class='form-group'>
        {{ Form::submit('Dodaj kurs', ['class' => 'btn btn-primary']) }}
        <a href="/course" class="btn btn-info">Cofnij</a>
    </div>
    
 
    {{ Form::close() }}
 
</div>
 
@stop