@extends('layouts.master')
 
@section('title') Dodawanie Lekcji @stop
 
@section('content')

<div class='col-lg-4 col-lg-offset-4'>
 
 
    <h1><i class='fa fa-user'></i> Dodaj lekcję dla {{$courseName->name}}</h1>
 
    {{ Form::open(['role' => 'form', 'url' => '/lesson', 'files'=>true]) }}
 
    <div class='form-group'>
        {{ Form::label('name', 'Nazwa') }}
        {{ Form::text('name', null, ['placeholder' => 'Nazwa', 'class' => 'form-control']) }}
    </div>

    
    <div class='form-group'>
        {{ Form::label('content', 'Treść') }}
        {{ Form::textarea('content', null, ['placeholder' => 'Treść lekcji', 'rows'=>"10", 'class' => 'form-control']) }}
    </div>
    
    {{ Form::hidden('course_id', $courseName->id) }}

    @if(Session::has('success'))
          <div class="alert-box success">
          <h2>{{ Session::get('success') }}</h2>
          </div>
        @endif
        
        <h3><i class='fa fa-folder-open'></i>Dodawanie plików</h3>
        
         <div class="control-group">
          <div class="controls">
          {{ Form::file('image') }}
	  <p class="errors">{{$errors->first('image')}}</p>
	@if(Session::has('error'))
	<p class="errors">{{ Session::get('error') }}</p>
	@endif
        </div>
        </div>

 
     
    <div class='form-group' id="success">
        {{ Form::submit('Dodaj lekcję', ['class' => 'btn btn-primary']) }}
    </div>
 
    {{ Form::close() }}

 
</div>


 
@stop
