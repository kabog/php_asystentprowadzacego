@extends('layouts.master')
 
@section('title') Login @stop
 
@section('content')

<div class='col-lg-4 col-lg-offset-4'>
 

	@if (Auth::check() )
	 Zalogowany jako {{ Auth::user()->getFullName(); }}
	 <br>
	 <a href="/logout" class="btn btn-default pull-right">Wyloguj</a>

	@else
	
			 <h1><i class='fa fa-lock'></i> Logowanie</h1>
		 
		    {{ Form::open(['role' => 'form']) }}
		 
		    <div class='form-group'>
		        {{ Form::label('login', 'Login') }}
		        {{ Form::text('login', null, ['placeholder' => 'Login', 'class' => 'form-control']) }}
		    </div>
		 
		    <div class='form-group'>
		        {{ Form::label('password', 'Hasło') }}
		        {{ Form::password('password', ['placeholder' => 'Hasło', 'class' => 'form-control']) }}
		    </div>
		 
		    <div class='form-group'>
		        {{ Form::submit('Zaloguj się', ['class' => 'btn btn-primary']) }} 
		    </div>
		 
		    {{ Form::close() }}
		    
		    	 <a href="/register" class="btn btn-info">Załóż konto</a>
	 			<a href="/" class="btn btn-info">Powrót</a>
		
	@endif
 
    @if ($errors->has())
        @foreach ($errors->all() as $error)
            <div class='bg-danger alert'>{{ $error }}</div>
        @endforeach
    @endif
 
</div>

@stop