@extends('layouts.master')
 
@section('title') Tworzenie konta @stop
 
@section('content')
 
<div class='col-lg-4 col-lg-offset-4'>
 
    @if ($errors->has())
        @foreach ($errors->all() as $error)
            <div class='bg-danger alert'>{{ $error }}</div>
        @endforeach
    @endif
    
    
 
    <h1><i class='fa fa-users'></i> Tworzenie konta</h1>
 
    {{ Form::open(['role' => 'form', 'url' => '/register']) }}
 
    <div class='form-group'>
        {{ Form::label('first_name', 'Imię') }}
        {{ Form::text('first_name', null, ['placeholder' => 'Imię', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('last_name', 'Nazwisko') }}
        {{ Form::text('last_name', null, ['placeholder' => 'Nazwisko', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('login', 'Login') }}
        {{ Form::text('login', null, ['placeholder' => 'Login', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) }}
    </div>
    
    <div class='form-group'>
        {{ Form::label('specialization', 'Kierunek studiów') }}
        {{ Form::text('specialization', null, ['placeholder' => 'Kierunek studiów', 'class' => 'form-control']) }}
    </div>
 
    <div class='form-group'>
        {{ Form::label('password', 'Hasło') }}
        {{ Form::password('password', ['placeholder' => 'Hasło', 'class' => 'form-control']) }}
    </div>
 
 
    <div class='form-group'>
        {{ Form::submit('Załóż', ['class' => 'btn btn-primary']) }}
         <a href="/" class="btn btn-info">Powrót</a>
    </div>
 
    {{ Form::close() }}
 
</div>
 
@stop