@extends('layouts.master')
 
@section('title') Login @stop
 
@section('content')

    @if ($errors->has())
        @foreach ($errors->all() as $error)
            <center><div class='bg-danger alert'>{{ $error }}</div></center>
        @endforeach
    @endif

<div class="container tabbable">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-md-12 column">
				
					<ul class="nav nav-tabs">
						 <li class="active"><a href="#Home" class="btn btn-danger" data-toggle="tab">Strona domowa</a></li>
						<li><a href="#Zasady" class="btn btn-warning" data-toggle="tab">Zasady zaliczenia</a></li>
						
						 <li class="dropdown">
							 <a href="#" data-toggle="dropdown" class="btn btn-success dropdown-toggle">Kursy prowadzącego/ Wyniki<strong class="caret"></strong></a>
				
							<ul class="dropdown-menu" role="menu">
								@foreach ($courses as $course)
								
									@if(Auth::check())
										<li><a href="#kursy_{{ $course->id }}" data-toggle="tab">{{ $course->name }}</a></li>
		        					@else
		        						<li><a href="/login">{{ $course->name }}</a></li>
	        						@endif
		        					
	        					@endforeach
        					
								<li role="presentation" class="divider"></li>
								@if (Auth::check() && Auth::user()->isAdmin())
								<li>
									<a href="/course">Modyfikuj kurs/lekcje</a>
								</li>
								@endif
								
							</ul>
						</li>
<!------------------------------   USTAWIENIA DODATKOWE  ---------------------------------------->						
						<li class="dropdown pull-right">
							 <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Ustawienia<strong class="caret"></strong></a>
				
							<ul class="dropdown-menu">
								<li>
									@if (Auth::check() )
									 <a href="/logout">Wyloguj</a>
									@else
									<a href="login">Logowanie</a>
									@endif
								</li>
								@if (Auth::check() && Auth::user()->isAdmin())
								<li>
									<a href="/user">Użytkownicy</a>
								</li>
								@endif
								
							</ul>
						</li>
						<li class="pull-right">
							<a>@if (Auth::check() )
							Zalogowany jako {{ Auth::user()->getFullName(); }} 
							@endif</a>
							
						</li >
					</ul>
					
					
<!------------------------------   ZDJECIE I CYTAT  ---------------------------------------->	
					
										
				<div class="tab-content">
			    <div class="tab-pane active" id="Home">  
			    <div id="mycarousel" class="carousel slide" data-ride="carousel">
				    <div class="carousel-inner">
				        <div class="item active">
			    <img alt="140x140" src="http://lorempixel.com/1150/140/" class="img-thumbnail" />
			       <div class="carousel-caption">
			       
			       
					   <style>
			  		 p.wlasne {
					    font-size: 45px;
					    text-align: right;
					    font-style: italic;
					    color: black;
						}   
					   </style>
			           <p class="wlasne">mgr inż. Ziemniaczek</p> 
			       
			           </div>
			        </div>
			    </div>
			</div>  
			<blockquote>
				<p>
					The beginning of knowledge is the discovery of something we do not understand.
				</p> <small>Frank Herbert</small>
			</blockquote>
			
			
				
				
<!------------------------------   OGLOSZENIA ---------------------------------------->		
				
			<div class="row clearfix">				
		<div class="col-md-6 column">
			<h2><i class='fa fa-thumbs-up'></i> Ogłoszenia <i class='fa fa-thumbs-down'></i></h2>
			@if (Auth::check())
			<ul>
				@foreach ($notices as $notice )
				<li>
					{{$notice->content}} [{{$notice->updated_at }}] 	@if(Auth::user()->isAdmin())
                    {{ Form::open(['url' => '/notice/' . $notice->id, 'method' => 'DELETE']) }}
                    {{ Form::submit('Usuń', ['class' => 'btn btn-danger btn-xs'])}}
                    {{ Form::close() }}
				@endif
				
				</li>
				@endforeach
			</ul>
			
			@else
			Żeby widzieć ogłoszenia musisz być zalogowany!
			@endif
			
			@if (Auth::check() && Auth::user()->isAdmin())
			
					<a href="/notice/create" class="btn btn-info pull-right">Dodaj ogłoszenie</a>
			@endif
			
		</div>
<!------------------------------   INFORMACJE O ASYSTENCIE  ---------------------------------------->	
				
				
		<div class="col-md-6 column">
			<div class="row clearfix">
				<div class="col-md-4 column">
					<img alt="140x140" src="http://lorempixel.com/140/140/" />
				</div>
				<div class="col-md-6 column">
			<h2><i class='fa fa-user'></i> Kontakt</h2>
						@foreach ( $extras as $extra)
						{{$extra->informations}}
						@endforeach
						@if (Auth::check() && Auth::user()->isAdmin())
							<br>
                 			 <a href="/extra/{{$extra->id}}/edit" class="btn btn-info pull-left" style="margin-right: 3px;">Edytuj</a>
                		@endif
					
				</div>
			</div>
		</div>
	</div>
			    </div>
			    
<!------------------------------   ZASADY ZALICZENIA  ---------------------------------------->	
			
			    <div class="tab-pane" id="Zasady">
			     <h2><i class='fa fa-thumbs-up'></i> Zasady zaliczenia przedmiotu <i class='fa fa-thumbs-down'></i></h2>
			 <ul>
				@foreach ( $extras as $extra)
				{{$extra->rules}}
				@endforeach
				@if (Auth::check() && Auth::user()->isAdmin())
                  <a href="/extra/{{$extra->id}}/edit" class="btn btn-info pull-left" style="margin-right: 3px;">Edytuj</a>
                @endif
			</ul>
			    </div>
			    
<!------------------------------   LEKCJE ---------------------------------------->				    
			    @foreach ($courses as $course)    
        		  <div class="tab-pane" id="kursy_{{ $course->id }}"> 
        		  
        		 
     <div class="col-lg-10 col-lg-offset-1">
 
    <h1><i class="fa fa-list navbar-left"></i>  Kursy prowadzącego: </h1>
    	<h3>{{ $course->name }}</h3>
        	
        		  @foreach ($lessons as $lesson)
		         <li>
        		 @if( $lesson->course_id == $course->id)
 			<h4><a href="lesson/{{$lesson->id}}">{{$lesson->name}}</a>, Aktualizacja: {{$lesson->updated_at }} </h4>
        		@endif
        		 </li>
        		@endforeach
			    </div>
			    </div>
        		@endforeach
       
			  	</div>
			  	</div>
				
				</div>
			</div>
			
		</div>
	</div>

 
@stop