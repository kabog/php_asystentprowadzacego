<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>@yield('title') | User Admin</title>
 
 		<link rel="stylesheet" href="{{ URL::asset('bootstrap.min.css') }}">
 	    <link rel="stylesheet" href="{{ URL::asset('font-awesome.css') }}">
		<script src="{{ URL::asset('jquery-1.11.2.min.js') }}"></script>
 		<script src="{{ URL::asset('bootstrap.min.js') }}"></script>
        <style>
            body {
                margin-top: 2%;
            }
        </style>
    </head>
    <body>
        <div class='container-fluid'>
            <div class='row'>
                @yield('content')
                
            </div>
        </div>
    </body>
</html>