@extends('layouts.master')
 
@section('title') Edycja zasad @stop
 
@section('content')
 
<div class='col-lg-4 col-lg-offset-4'>
 
    @if ($errors->has())
        @foreach ($errors->all() as $error)
            <div class='bg-danger alert'>{{ $error }}</div>
        @endforeach
    @endif
	
	
	
	<h3><i class='fa fa-education'></i> Edycja Informacji o prowadzącym</h3>
 
 	{{ Form::model($informations, ['role' => 'form', 'url' => '/extra/' . $informations->id, 'method' => 'PUT']) }}
 
    <div class='form-group'>
        {{ Form::textarea('informations', null , ['placeholder' => 'Informacje', 'class' => 'form-control']) }}
    </div>
 <!--   <div class='form-group'>
        {{ Form::submit('Edytuj', ['class' => 'btn btn-primary']) }}
    </div>
     {{ Form::close() }} --> 
	
 
    <h3><i class='fa fa-education'></i> Edycja Zasad Zaliczenia Przedmiotu </h3>
 
 <!--	{{ Form::model($rules, ['role' => 'form', 'url' => '/extra/' . $rules->id, 'method' => 'PUT']) }} -->
 
    <div class='form-group'>
        {{ Form::textarea('rules', null , ['placeholder' => 'Zasady', 'class' => 'form-control']) }}
    </div>
    <div class='form-group'>
        {{ Form::submit('Edytuj', ['class' => 'btn btn-primary']) }}
         <a href="/" class="btn btn-info">Powrót na stronę główną</a>
    </div>
 
    {{ Form::close() }}
</div>
@stop