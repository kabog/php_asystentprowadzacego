<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersCourses extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_courses', function($table)
		{
			$table->integer('user_id');
			$table->integer('course_id');
			$table->boolean('access');
		});
	}

	public function down()
	{
		Schema::drop('users_courses');
	}

}
