<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notices extends Migration {


	public function up()
	{
		Schema::create('notices', function($table)
		{
			$table->increments('id');
			$table->longText('content');
			$table->timestamps('date');
		});
	}

	public function down()
	{
		Schema::drop('lessons');
	}

}
