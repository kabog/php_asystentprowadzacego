<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Extras extends Migration {

	public function up()
	{
		Schema::create('extras', function($table)
		{
			$table->increments('id');
			$table->longText('rules');
			$table->longText('informations');
			$table->longText('citation');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('extras');
	}

}
