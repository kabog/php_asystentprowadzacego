<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->boolean('permissions');
			$table->boolean('active');
			$table->string('login')->unique();
			$table->string('password');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('specialization');
			$table->string('remember_token');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('users');
	}

}
