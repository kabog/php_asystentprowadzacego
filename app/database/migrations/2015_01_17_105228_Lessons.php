<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lessons extends Migration {

	public function up()
	{
		Schema::create('lessons', function($table)
		{
		$table->increments('id');
		$table->integer('course_id');
		$table->string('name');
		$table->longText('content');
		$table->timestamps('date');
		});
	}

	public function down()
	{
		Schema::drop('lessons');
	}

}
