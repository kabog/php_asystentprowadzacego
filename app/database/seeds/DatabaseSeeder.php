<?php

class DatabaseSeeder extends Seeder {

	 public function run()
    {
        $this->call('UserTableSeeder');

        $this->command->info('User table seeded!');
    }

}

class UserTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('users')->insert([
    	'login'   => 'admin',
    	'password'   => Hash::make('admin'),
    	'permissions' => 1,
    	'active' => 1,
    	'first_name' => 'admin',
    	'last_name' => 'admin',
    	'email' => 'admin@to.com'
    	]);
    	
    	DB::table('users')->insert([
    	'login'   => '1',
    	'password'   => Hash::make('1'),
    	'permissions' => 0,
    	'specialization' => 'IS',
    	'active' => 1,
    	'first_name' => 'Adi',
    	'last_name' => 'Stasiu',
    	'email' => 'mail@to.com' 	
		]);
    	
    	DB::table('extras')->insert([
    	'rules'   => 'I tak nie zdacie!',
    	'informations' => 'Jestem wesoły Romek!'
    	]);
    }


}
