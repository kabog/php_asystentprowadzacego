<?php

class NoticeController extends \BaseController {

	public function index()
	{
	}

	public function create()
	{
		return View::make('notice.index');
	}


	public function store()
	{
		$notice = new Notice;
		
		$notice->content = Input::get('content');
		
		$notice->save();
		
		return Redirect::to('/');
	}


	public function edit($id)
	{
		//
	}


	public function update($id)
	{
		//
	}


	public function destroy($id)
	{
		Notice::destroy($id);
		
		return Redirect::to('/');
	}


}
