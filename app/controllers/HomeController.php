<?php

class HomeController extends BaseController {

	
	public function getIndex()
	{
		$courses = Course::all();
		$lessons = Lesson::all();
		$notices = Notice::all();
		$extras = Extra::all();
		return View::make('home.index', ['courses' => $courses, 'lessons' => $lessons, 'notices'=>$notices, 'extras'=>$extras]);
	}
	
	public function postLogin()
	{
		$userdata = array(
				'login' => Input::get('login'),
				'password' => Input::get('password')
		);
	
		$existing = User::where('login', '=', Input::get('login') )->get();
		if(count($existing) == 0){
			return Redirect::to('/login')->withInput()->withErrors('Podany użytkownik nie istnieje');
		}
		
		
		if(count($existing) == 1 && $existing[0]->active == false){
			return Redirect::to('/login')->withInput()->withErrors('Podany użytkownik nie jest aktywny');
		}
	
		if (Auth::attempt($userdata))
		{
			//return Redirect::intended('/');
			return Redirect::to('/');
		}  else{
			return Redirect::back()
				->withInput()
				->withErrors('That username/password combo does not exist.');
		}
		
	
		
	}
	
	public function getRegister()
	{
		return View::make('home.register');
		//return Redirect::to('/');
	}
	
	public function postRegister(){
		{
			
			$existing = User::where('login', '=', Input::get('login') )->get();
		
			if(count($existing) > 0){
				return Redirect::to('/register')->withInput()->withErrors('Podany użytkownik istnieje');
			}
				

			$user = new User;
			$user->first_name = Input::get('first_name');
			$user->last_name  = Input::get('last_name');
			$user->login   = Input::get('login');
			$user->specialization   = Input::get('specialization');
			$user->email      = Input::get('email');
			$user->password   = Hash::make(Input::get('password'));
		
			$user->save();
		
			return Redirect::to('/')->withErrors('Poczekaj na akceptację.');
		}
		
	}
	
	
	public function getLogin()
	{
		return View::make('home.login');
		//return Redirect::to('/');
	}
	
	public function getLogout()
	{
		Auth::logout();
	
		return Redirect::to('/');
	}
	
	public function getActivate($id)
	{
    	$user = User::find($id);
 
    	$user->active = true;
    	$user->save();
    	return Redirect::to('/user');
	}

}
