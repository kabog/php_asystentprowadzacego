<?php

class LessonController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}
	
	public function index()
	{
		//
	}

	public function create()
	{
		//
	}

	public function store()
	{
		$lesson = new Lesson;
		$lesson->course_id = Input::get('course_id');
		$lesson->name = Input::get('name');
		$lesson->content = Input::get('content');
		$lesson->created_at = new DateTime();
		$lesson->updated_at = new DateTime();
		$lesson->save();

		$file = array('image' => Input::file('image'));
		 
		if(Input::file('image') != NULL){
			if (Input::file('image')->isValid()) {
				$destinationPath = 'public/assets/lesson_'.$lesson->id; // upload path
				$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
				Input::file('image')->move($destinationPath, Input::file('image')->getClientOriginalName()); // uploading file to given path
				// sending back with message
				Session::flash('error', 'Upload successfully');
				return Redirect::to('/')->withErrors('Upload successfully');
			}
			else {
				// sending back with error message.
				Session::flash('error', 'uploaded file is not valid');
				return Redirect::to('/');
			}
		}

		
		return Redirect::to('/course');
	}


	public function edit($id)
	{
		
	}


	public function update($id)
	{
		
	}

	public function destroy($id)
	{
		Lesson::destroy($id);
		
		$filename = "assets/lesson_".$id;
		
		if (File::exists(public_path($filename))) {
			
			File::deleteDirectory("public/assets/lesson_$id");

			
		}
		
		
		
		return Redirect::to('/course');
	}

	public function show($id){
	if(!Auth::check()){
			return Redirect::back()
			->withInput()
			->withErrors('Nie jesteś zalogowany.');
		}
		
	
		$filename = "assets/lesson_".$id;
		
		if (File::exists(public_path($filename))) {
			
			$exist = "The file $filename exists";
			
			if(count(File::files(public_path($filename)))>0){
					$exist = basename(File::files(public_path($filename))[0]);
					$filename .= "/$exist";
			}
			
		} else {
			$exist = false;
		}
		
		return View::make('lesson.index', ['lesson' => Lesson::find($id), 'file'=>$filename,'attachmentExist'=>$exist]);
	}

}
