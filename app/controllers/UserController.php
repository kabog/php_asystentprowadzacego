<?php
 
class UserController extends \BaseController {
 
    public function __construct()
    {
        $this->beforeFilter('auth');
    }
 
 
    public function index()
    {
    	if(!Auth::user()->isAdmin()){
    		return Redirect::back()
				->withInput()
				->withErrors('Nie masz uprawnień.');
    	}
    	
    	
    	$users = User::all();
 
        return View::make('user.index', ['users' => $users]);
    }
 
    public function create()
    {
        return View::make('user.create');
    } 
 
    public function edit($id)
    {
        $user = User::find($id);
 
        return View::make('user.editAdmin', [ 'user' => $user ]);
        
    }

    public function update($id)
    {
        $user = User::find($id);
 
       $user->first_name = Input::get('first_name');
        $user->last_name  = Input::get('last_name');
        $user->login   = Input::get('login');
        $user->email      = Input::get('email');
        $user->password   = Hash::make(Input::get('password'));
 
        $user->save();
 
        return Redirect::to('/user');
    }
 

    public function destroy($id)
    {
        User::destroy($id);
 
        return Redirect::to('/user');
    }

 
}