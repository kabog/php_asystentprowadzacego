<?php

class ExtraController extends \BaseController {


	public function index()
	{
	}


	public function create()
	{
		return View::make('extra.index');
	}

	public function store()
	{
		$extra = new Extra;
		
		$extra->informations = Input::get('informations');
		$extra->rules = Input::get('rules');
		
		
		$rules->save();
		
		return Redirect::to('/');
	}


	public function edit($id)
	{
		$rules = Extra::find($id);
		$informations = Extra::find($id);
 
        return View::make('extra.editRule', [ 'rules' => $rules ], ['informations' => $informations]);
	}


	public function update($id)
	{
		$extra = Extra::find($id);
 
       $extra->rules = Input::get('rules');
       $extra->informations = Input::get('informations');
       
        $extra->save();
 
        return Redirect::to('/');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
