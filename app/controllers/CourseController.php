<?php

class CourseController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth');
	}
	
	public function index()
	{
		if(!Auth::user()->isAdmin()){
			return Redirect::back()
			->withInput()
			->withErrors('Nie masz uprawnień.');
		}
		 
		 
		$courses = Course::all();
		
		$lessons = Lesson::all();
		
		return View::make('course.index', ['courses' => $courses,'lessons' => $lessons]);
		
		
	}

	public function create()
    {
    	return View::make('course.create');
    }

	public function store()
	{
		$course = new Course;
		
		$course->name = Input::get('name');
		
		$course->save();
		
		return Redirect::to('/course');
		
	}


	public function edit($id)
	{
		$course = Course::find($id);
		return View::make('course.addLesson', ['courseName'=>$course]);
	}

	public function update($id)
	{
		
	}

    public function destroy($id)
    {	
        Course::destroy($id);
        Lesson::where('course_id', '=', $id)->delete();
 
        return Redirect::to('/course');
    }
    
    public function upload() {

    }
    


}
