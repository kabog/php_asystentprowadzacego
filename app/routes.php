<?php

/*
 * |-------------------------------------------------------------------------- | Application Routes |-------------------------------------------------------------------------- | | Here is where you can register all of the routes for an application. | It's a breeze. Simply tell Laravel the URIs it should respond to | and give it the Closure to execute when that URI is requested. |
*/

Route::resource('/user', 'UserController');
Route::resource('/course', 'CourseController');
Route::resource('/lesson', 'LessonController');
Route::resource('/notice', 'NoticeController');
Route::resource('/extra', 'ExtraController');
Route::controller('/', 'HomeController');

